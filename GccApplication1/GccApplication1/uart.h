/*
 * uart.h
 *
 * Created: 19/11/2015 7:35:02 PM
 *  Author: danie
 */ 


#ifndef UART_H_
#define UART_H_

#include "common.h"

// Define baud rate
#define USART_BAUDRATE 38400
#define BAUD_PRESCALE (((F_CPU / (USART_BAUDRATE * 16UL))) - 1)

int uart_init();
int uart_send();


#endif /* UART_H_ */