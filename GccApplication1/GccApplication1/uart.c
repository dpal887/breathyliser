/*
 * uart.c
 *
 * Created: 19/11/2015 7:34:47 PM
 *  Author: daniel
 */ 
#include "uart.h"

int uart_init(){
	UBRRL = BAUD_PRESCALE;// Load lower 8-bits into the low byte of the UBRR register
	UBRRH = (BAUD_PRESCALE >> 8); 
	 /* Load upper 8-bits into the high byte of the UBRR register
    Default frame format is 8 data bits, no parity, 1 stop bit
	to change use UCSRC, see AVR datasheet*/ 

	// Enable receiver and transmitter and receive complete interrupt 
	UCSRB = ((1<<TXEN)|(1<<RXEN) | (1<<RXCIE));
	return UCSRB == ((1<<TXEN)|(1<<RXEN) | (1<<RXCIE));
}

int uart_send(){
	
	return 1;
}