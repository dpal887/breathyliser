/*
 * adc.c
 *
 * Created: 17/11/2015 8:51:42 PM
 *  Author: danie
 */ 

#include "common.h"
#include "adc.h"

int init_adc(){
    //Div-fact = 32
    ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS0);
    //Set ADC mux to PC5
    ADMUX=0x05;
    //Return from function with success
    if(ADCSRA == ((1<<ADEN) | (1<<ADPS2) | (1<<ADPS0))){
      return 1;
    }else{
      return 0;
    }
  }
  
unsigned int read_adc(){
	
	unsigned int adc_value = 0;
	
	ADCSRA |= (1<<ADSC);
	//Blocking call to wait for adc data
	while (ADCSRA & (1<<ADSC));
	//Store value of adc
	adc_value = ADCW;
	//Return read value
	return adc_value;
}