/*
 * adc.h
 *
 * Created: 17/11/2015 8:51:56 PM
 *  Author: danie
 */ 


#ifndef ADC_H_
#define ADC_H_

#define PORT_ON(port,pin) port |= (1<<pin)
#define PORT_OFF(port,pin) port &= ~(1<<pin)

int init_adc();
unsigned int read_adc();


#endif /* ADC_H_ */