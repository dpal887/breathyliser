/*
 * common.h
 *
 * Created: 17/11/2015 8:52:40 PM
 *  Author: danie
 */ 


#ifndef COMMON_H_
#define COMMON_H_

#include "adc.h"
#include <avr/io.h>
#include "stdlib.h"
#define F_CPU 8000000UL  // 8 MHz

void delay(unsigned char);
void init_lcd();
void lcd_cmd(unsigned char);
void lcd_data(unsigned char);
void lcd_send_string(char*);



#endif /* COMMON_H_ */