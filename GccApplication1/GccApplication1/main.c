
#include "common.h"
#include "uart.h"

int uart_init();
//Main Code
int main()
{
	unsigned int reading_from_sensor;
	
	DDRB=0xff;    //set PORTB as out put
	
	DDRD=0b01111100;   //Set PD.4,5 and 6 as Output

	//Give Inital Delay for LCD to startup as LCD is a slower Device
	delay(2);
	init_lcd();
	delay(2);
	init_adc();
	while(1)
	{
		reading_from_sensor = read_adc();
		lcd_cmd(0x80);       //Goto Line-1,first position
		lcd_send_string("Voltage: ");
		lcd_send_string(itoa(reading_from_sensor, "10",10));
		lcd_cmd(0x01);     //Clear the lcd
		delay(2);
		
	}
}

//LCD function
/*------------------------------------------------------------------------------------------------------------*/

//Function for sending commands to LCD
void lcd_cmd(unsigned char command)

{
	
	int temp = 0;
	//Put command on the Data Bus
	PORTB = command;
	temp = command & 0b11000000;
	//Enable LCD for command writing
	PORTD = 0b00010000+ (temp/8);

	//Allow delay for LCD to read the databus
	delay(1);

	//Disable LCD again
	PORTD = 0b00000000+ (temp/8);

	//Allow some more delay
	delay(1);
}

//Function for sending Data to LCD
void lcd_data(unsigned char data)
{
	//Put data on Data Bus
	int temp = 0;
	
	PORTB= data;
	temp = data & 0b11000000;

	//Set R/S (Regiter Select) to High, and Enable to High
	PORTD = 0b00110000 + (temp/8);

	//Allow for delay
	delay(2);

	//Disable LCD again
	PORTD = 0b00100000 + (temp/8);

	//Allow for some more delay
	delay(2);
}

//Function to send String to LCD
void lcd_send_string(char* string)
{
	while(*string)
	{
		//Send value of pointer as data to LCD
		lcd_data(*string);
		//Increment string pointer
		string++;
	}
}




//Function to Initilise LCD
void init_lcd()
{
	//Setup both lines of LCD
	lcd_cmd(0x38);
	//Set Cursor off - Enable LCD
	lcd_cmd(0x0E);
	//Clear Screen
	lcd_cmd(0x01);
	//Goto first position
	lcd_cmd(0x80);
}
/*----------------------------------------------------------------------------------------------------------*/

//Delay function

void delay(unsigned char dtime)
{
	int i,j;
	for(i=0;i<=dtime;i++)
	{
		for(j=0;j<5000;j++);
	}}